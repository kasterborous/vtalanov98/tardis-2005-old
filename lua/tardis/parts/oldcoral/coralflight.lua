local PART={}
PART.ID = "oldcoralflight"
PART.Name = "2005 Coral Flight Lever"
PART.Model = "models/doctorwho1200/tennant/rewrite/flightlever.mdl"
PART.AutoSetup = true
PART.Animate = true

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/tennant/rewrite/flightlever.wav" ))
		self.exterior:ToggleFlight()
	end
end

TARDIS:AddPart(PART,e)