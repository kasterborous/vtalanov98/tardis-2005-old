local PART={}
PART.ID = "oldcorallever3"
PART.Name = "2005 Coral Lever 3"
PART.Model = "models/doctorwho1200/tennant/rewrite/lever3.mdl"
PART.AutoSetup = true
PART.Animate = true

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/tennant/rewrite/lever3.wav" ))
	end
end

TARDIS:AddPart(PART,e)