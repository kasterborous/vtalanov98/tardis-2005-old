local PART={}
PART.ID = "oldcoraldp"
PART.Name = "2005 Coral Directional Pointer"
PART.Model = "models/doctorwho1200/tennant/rewrite/directionalpointer.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 2.0

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/tennant/rewrite/directionalpointer.wav" ))
	end
end

TARDIS:AddPart(PART,e)