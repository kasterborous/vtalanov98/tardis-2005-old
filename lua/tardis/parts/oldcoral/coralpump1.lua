local PART={}
PART.ID = "oldcoralpump1"
PART.Name = "2005 Coral Pump 1"
PART.Model = "models/doctorwho1200/tennant/rewrite/pump1.mdl"
PART.AutoSetup = true
PART.Animate = true

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/tennant/rewrite/pump1.wav" ))
	end
end

TARDIS:AddPart(PART,e)