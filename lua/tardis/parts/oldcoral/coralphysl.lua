local PART={}
PART.ID = "oldcoralphysl"
PART.Name = "2005 Coral Physical Lock"
PART.Model = "models/doctorwho1200/tennant/rewrite/flightlever.mdl"
PART.AutoSetup = true
PART.Animate = true

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/tennant/rewrite/flightlever.wav" ))
	end
end

TARDIS:AddPart(PART,e)