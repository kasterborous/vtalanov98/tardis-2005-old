local PART={}
PART.ID = "oldcoralbell"
PART.Name = "2005 Coral Bell"
PART.Model = "models/doctorwho1200/tennant/rewrite/bell.mdl"
PART.AutoSetup = true
PART.Animate = true

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/tennant/rewrite/bell.wav" ))
	end
end

TARDIS:AddPart(PART,e)