local PART={}
PART.ID = "oldcorallever2"
PART.Name = "2005 Coral Lever 2"
PART.Model = "models/doctorwho1200/tennant/rewrite/lever2.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 1.5

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/tennant/rewrite/lever2.wav" ))
	end
end

TARDIS:AddPart(PART,e)