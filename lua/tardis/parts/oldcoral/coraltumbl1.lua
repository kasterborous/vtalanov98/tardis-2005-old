local PART={}
PART.ID = "oldcoraltumbl1"
PART.Name = "2005 Coral Tumblers 1"
PART.Model = "models/doctorwho1200/tennant/rewrite/tumblers1.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 2.0

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/tennant/rewrite/tumblers1.wav" ))
	end
end

TARDIS:AddPart(PART,e)