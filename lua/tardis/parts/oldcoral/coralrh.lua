local PART={}
PART.ID = "oldcoralrh"
PART.Name = "2005 Coral Rheostat"
PART.Model = "models/doctorwho1200/tennant/rewrite/rheostat.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 0.8

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/tennant/rewrite/rheostat.wav" ))
	end
end

TARDIS:AddPart(PART,e)