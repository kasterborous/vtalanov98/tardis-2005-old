local PART={}
PART.ID = "oldcoralthr"
PART.Name = "2005 Coral Throttle"
PART.Model = "models/doctorwho1200/tennant/rewrite/throttle.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 3.0

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/tennant/rewrite/throttle.wav" ))
           local ext=self.exterior
           if ext:GetData("vortex") then
                   ext:Mat()
           else
                   ext:Demat()
           end
        end
end

TARDIS:AddPart(PART,e)