local PART={}
PART.ID = "oldcorallever5"
PART.Name = "2005 Coral Lever 5"
PART.Model = "models/doctorwho1200/tennant/rewrite/lever5.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 2.0

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/tennant/rewrite/lever5.wav" ))
		self.exterior:ToggleFloat()
	end
end

TARDIS:AddPart(PART,e)